###########################################################
#    Damped SHO fit of a time series sample:               
#    A*cos(wt + phi)*e^(0.5*gamma*wt) + b + s*wt/(2*pi)    
#    wt = 2*pi*n/N                                         
###########################################################

########   file and parameter guesses   ########


# y=kx+m

m = 1
b = 0.1



################################################

# start of the fitting script

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import leastsq

# fit log z vs log n
Q = np.array([76.16,95.37,102.68,83.83,71.08,91.55])
x = np.array([140,240,340,440,540,640])
y = np.array([3.298,2.69,2.325,2.08,1.9,1.759])
z = np.log(y)
n = np.log(x)

# define the resonance function (amp and phase relative to RLC resonance)

def Resonance(n, m, b):
    return m*n+b

# define a residual magnitude 
    
def ResErr(m, b):
    return np.absolute(Resonance(n, m, b)-z)

# check the function

fz=Resonance(n, m, b)

plt.xlabel("log x")
plt.ylabel("log y")
plt.title("Fit Comparison -- Guesses")
plt.plot(n,z,"ro")
plt.plot(n,fz,"b")
plt.grid()
plt.show()

# define a "lambda" function object to call with a parameter set
#     required by the scipy.optimize.leastsq routine

errfunc = lambda p: ResErr(p[0],p[1])

# run the least squares (full_output = 0)

p0 = [m, b]               # parameter array
p0, flag = leastsq(errfunc, p0[:])          # call scipy.optimize.leastsq

# show the optimized parameters

print()
print("log slope = %10.4F" % p0[0])
print("log intercept = %14.6E" % p0[1])

# show the fit with the data

[m, b] = p0
fz=Resonance(n, m, b)

fig = plt.figure()
plt.xlabel("log (pF)")
plt.ylabel("log (Mhz)")
plt.title("Fit Comparison -- Least-Squares")
plt.plot(n,z,"ro")
plt.plot(n,fz,"b")
plt.grid()
plt.show()

# check the fit residuals

ez=ResErr(m, b)
print()
print(" RMS log Error = %9.3E" % np.sqrt(np.mean(ez*ez)))


# Veractor capacitance changes
cr = 100
fr = 4.559
f = np.array([3.51,3.86,4.06,4.23,4.33,4.38,4.40,4.42,4.43,4.44,4.44,4.45,4.45,4.45,4.457,4.459])
c = ((f / fr) ** (1 / p0[0])) * cr
v = np.arange(0,16,1)

plt.title("C vs V : varactor diode")
plt.grid()
plt.xlabel("Voltage (Volts)")
plt.ylabel("Capacitanc (pF)")
plt.plot(v,c,"bo",v,c,"b")
plt.show()


