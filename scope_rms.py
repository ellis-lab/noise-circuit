##########################################
# Minimal Python scope access 
##########################################

import numpy as np
import matplotlib.pyplot as plt
import pyvisa
from time import *


# open the DPO2014 (model "B" has differnt number)

tek = pyvisa.ResourceManager().open_resource("USB0::1689::927::C030124::0::INSTR")
print("  Scope found: "+tek.query('*IDN?'))
tek.write('HEADDER OFF')

###########################
# This function gets the selected channel's VRMS
###########################

def GetVRMS(chn):
    
    tek.write("measu:imm:source1 ch{:}".format(chn))
    tek.write("measu:imm:type rms")
    return float(tek.query("measu:imm:val?"))


###########################
# This gets the selected channel frequency from the scope trigger system
###########################

def GetFreq(chn):    # chan = 1, 2, 3, or 4

    tek.write("MEASU:IMM:SOURCE2 CH{:}".format(chn))
    tek.write("MEASU:IMM:TYPE freq")
    return float(tek.query('MEASU:IMM:VAL?'))


###########################
# This function gets the selected channel data from the scope, returning the
#     time sample step and the time series samples.
###########################

def GetCurve(chn):    # chan = 1, 2, 3, or 4

# set the data source, curve parameters, and curve encoding options

    tek.write("data:source ch{:}".format(chn))            # channel
    tek.write("data:composition singular_yt")             # simple y(t) data
    tek.write("data:encdg fastest")                       # lowest-level binary

# check for averaging, and use two bytes for the averaged waveforms...

    if tek.query("acquire:mode?").strip() == 'AVE':
        tek.write("data:width 2")
    else:
        tek.write('data:width 1')

# get the maximum number of samples available

    sz = int(tek.query('wfmoutpre:recordlength?'))
    

# specify the data range 
    tek.write("data:start 1")
    tek.write("data:stop {:}".format(sz))
   
# Get the parameters necessary for converting from binary curve data to
#     physical data with units. (Only one that are actually used.)

    BYT_N = int(tek.query('wfmoutpre:byt_n?'))
    XIN = float(tek.query('wfmoutpre:xincr?'))
    YMU = float(tek.query('wfmoutpre:ymult?'))

# Get trace data from scope into a numpy byte array. T

    if BYT_N==1:
        trace = np.array(tek.query_binary_values("curve?", datatype="b", is_big_endian=True),float)*YMU
    else:
        trace = np.array(tek.query_binary_values("curve?", datatype="h", is_big_endian=True),float)*YMU  

    return XIN,trace



##################################################################
# This is the main script that runs...
##################################################################
ch=1
y = np.array([])
for j in range(144):
#    print("  RMS Voltage = {:11.4E}".format(GetVRMS(ch)))
#    print("         Freq = {:11.4E}".format(GetFreq(ch)))
#    tek.write('trig:stop')
#    dt,y=GetCurve(ch)
#    tek.write('trig:start')
    y = np.append(y,GetVRMS(ch))
    print(j)
    sleep(30)  # was 300

# plot
plt.plot(y,linewidth=0.5)
plt.show()
np.savetxt("scope_rms1.dat", y , fmt="%12.5E")

# close the DPO2014

#tek.close()

