import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import leastsq
import pyvisa
import time

##################################################################
# First transfer waveform data and store in numpy array          #
##################################################################

# open the DPO2014B

tek = pyvisa.ResourceManager().open_resource("USB0::1689::927::C030124::0::INSTR")
print("  Scope found: "+tek.query('*IDN?'))
tek.write('HEADDER OFF')


###########################
# This function gets the selected channel's VRMS
###########################

def GetVRMS(chn):
    
    tek.write("measu:imm:source1 ch{:}".format(chn))
    tek.write("measu:imm:type rms")
    return float(tek.query("measu:imm:val?"))


###########################
# This gets the selected channel frequency from the scope trigger system
###########################

def GetFreq(chn):    # chan = 1, 2, 3, or 4

    tek.write("MEASU:IMM:SOURCE2 CH{:}".format(chn))
    tek.write("MEASU:IMM:TYPE freq")
    return float(tek.query('MEASU:IMM:VAL?'))


###########################
# This function gets the selected channel data from the scope, returning the
#     time sample step and the time series samples.
###########################

def GetCurve(chn):    # chan = 1, 2, 3, or 4

# set the data source, curve parameters, and curve encoding options

    tek.write("data:source ch{:}".format(chn))            # channel
    tek.write("data:composition singular_yt")             # simple y(t) data
    tek.write("data:encdg fastest")                       # lowest-level binary

# check for averaging, and use two bytes for the averaged waveforms...

    if tek.query("acquire:mode?").strip() == 'AVE':
        tek.write("data:width 2")
    else:
        tek.write('data:width 1')

# get the maximum number of samples available

    sz = int(tek.query('wfmoutpre:recordlength?'))
    

# specify the data range 
    tek.write("data:start 1")
    tek.write("data:stop {:}".format(sz))
   
# Get the parameters necessary for converting from binary curve data to
#     physical data with units. (Only one that are actually used.)

    BYT_N = int(tek.query('wfmoutpre:byt_n?'))           # data width of the waveform
    XIN = float(tek.query('wfmoutpre:xincr?'))           # horizontal points spacing
    YMU = float(tek.query('wfmoutpre:ymult?'))           # vertical scale factor in mV/div

# Get trace data from scope into a numpy byte array.

    if BYT_N==1:       # not averaging
        trace = np.array(tek.query_binary_values("curve?", datatype="b", is_big_endian=True),float)*YMU
    else:              # averaging
        trace = np.array(tek.query_binary_values("curve?", datatype="h", is_big_endian=True),float)*YMU  

    return XIN,trace


# This function selects an appropriate section of response to plot
#data is numpy array, dt is time increment, t is the scope's horizontal scale

def select(data,dt,t,pts):
    threshold=np.max(data) * 0.6
    idx=0
    size=np.size(data)
    while idx < size:
        if data[idx] >= threshold:
            x=idx + int(t/dt)
            return data[x:x+pts]
        idx += 1
    else:
        print("No valid data, check circuit!")



ch=1
rsp = ""
while rsp=="":
    dt,alldata=GetCurve(ch)
    plt.plot(alldata,linewidth=0.5)            #show the acquired data first
    plt.show()


    """pts=int(input('Enter points: '))
    t=float(input('Enter t: '))"""
    pts=20000
    t=2e-6


    data=select(alldata,dt,t,pts)
    n=np.arange(0,np.size(data))



    ##################################################################
    # Then fit the trace and output required parameters              #
    ##################################################################

    # manually supply parameter guesses...

    N = 300                 # resonance period (in sample points)
    gamma = -.03            # resonance damping factor
    A = .1                  # initial resoance amp
    phi = -2                # resonance phase
    b = 0                   # background bias
    s = 0                   # background slope (per cycle)



    # define the resonance function (amp and phase relative to RLC resonance)

    def Resonance(n, N, gamma, A, phi, b, s):
        cyc=n/N
        wt = 2*np.pi*cyc
        return A*np.cos(wt+phi)*np.exp(0.5*gamma*wt) + b + s*cyc



    # define a residual magnitude 

    def ResErr(N, gamma, A, phi, b, s):
        return np.absolute(Resonance(n, N, gamma, A, phi, b, s)-data)



    ##############################################################
    # Main script
    ##############################################################


    fz=Resonance(n, N, gamma, A, phi, b, s)

    plt.xlabel("Point Index")
    plt.ylabel("Position")
    plt.title("Fit Comparison -- Guesses")
    plt.plot(n,data,"ro")
    plt.plot(n,fz,"b")
    plt.grid()
    plt.show()



    # define a "lambda" function object to call with a parameter set
    # required by the scipy.optimize.leastsq routine
    errfunc = lambda p: ResErr(p[0],p[1],p[2],p[3],p[4],p[5])



    # run the least squares (full_output = 0)

    p0 = [N, gamma, A, phi, b, s]               # parameter array
    p0, flag = leastsq(errfunc, p0[:])          # call scipy.optimize.leastsq


    # show the optimized parameters
    Q = -1/p0[1]
    f = 1/(p0[0]*dt)
    print()
    print("  N_per = %10.4F" % p0[0])
    print("  gamma = %14.6E" % p0[1])
    print("      A = %14.6E" % p0[2])
    print("    phi = %14.6E" % p0[3])
    print("      b = %14.6E" % p0[4])
    print("      s = %14.6E" % p0[5])
    print("      Q = %14.6F" % Q)
    print("      f = %10.4E" % f)
    print("delta t = %11.4E" % dt)

    # show the fit with the data

    [N, gamma, A, phi, b, s] = p0
    fz=Resonance(n, N, gamma, A, phi, b, s)

    fig = plt.figure()
    plt.xlabel("Point Index")
    plt.ylabel("Position")
    plt.title("Fit Comparison -- Least-Squares")
    plt.plot(n,data,"ro")
    plt.plot(n,fz,"b")
    plt.grid()
    plt.show()

    # check the fit residuals

    ez=ResErr(N, gamma, A, phi, b, s)
    print()
    print(" RMS Error = %9.3E" % np.sqrt(np.mean(ez*ez)))
    print()

    plt.xlabel("Point Index")
    plt.ylabel("Deviation Magnitude")
    plt.title("Fit Residuals")
    plt.plot(n,ez)
    plt.grid()
    plt.show()
    rsp=input("  Return to go again > ")



print("##############################################################")
