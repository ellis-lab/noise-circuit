import numpy as np
import matplotlib.pyplot as plt


x=np.arange(-4,4,0.02)
y=np.sin(np.exp(x))
plt.plot(x,y)
plt.xlabel("x")
plt.ylabel("e$^x$",rotate)
plt.grid()
plt.show()
np.version
