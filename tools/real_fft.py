#########################################
# takes the FFT of a real time series data set, with windowing and padding
# plots the magnitudes and finds peaks
# primary test/development location C:\fred\MATH\FFTs\py_RealFFT
#########################################

# load some packages needed

from math import *                    # all math functions (not needed, but
import numpy as np                    # useful array operations
import matplotlib.pyplot as plt       # interactive graphics

# get the file name

#fname=input("File name? > ")
fname="temp.dat"

# supply the sample time for frequency units... 

dt=0.1         # known sample time (seconds will report Hz), or...
dt=0           # zero flags cycles per data period 

###############################################

# custom log-log transform plots

def plotfft():
    plt.plot(frq,np.abs(fd))
    plt.title("Real FFT Magnitude")
    plt.grid()
    if dt>0:
        plt.xlabel("Frequency (Hz)")
    else:
        plt.xlabel("Frequency (cycles per data length")
    plt.xscale("log")
    #plt.xlim(0.001,10)
    plt.yscale("log")
    #plt.ylim(1E-5,1E-2)
    plt.ylabel("RFFT Amp")
    plt.show()
    return 0

# read in the desired columns -- note: col 1 = array index 0
#ts=np.loadtxt(fname, usecols=(0,1,2,14), unpack="true", skiprows=1)

ts=np.loadtxt(fname, usecols=(0), unpack="true")

# "window" the data with a Gaussian

ns=np.size(ts)
rsz=4.0/ns            # Gaussian is exp(-4) ~ 2% at data boundaries

for j in range(ns):
    ts[j]=ts[j]*exp(-(rsz*float(j)-2.0)**2)

# pad to the next higher integer power of two for efficient FFT

nt=2**ceil(log(ns)/log(2))
ts=np.concatenate((ts,np.zeros(nt-ns)))

# take the transform

fd=np.fft.rfft(ts)

# isolated peaks should be Gaussians
# rescale so that peak heights are the spectral amplitudes
# the above scaling is only accurate for isolated peaks! 

sfac=8.0/ns/sqrt(pi)
fd=sfac*fd

# form the frequency scale factor

if dt>0:
    frqf=1/(nt*dt)               # actual frequency based on a given dt
else:
    frqf=float(ns)/float(nt)     # base on cycles per transform set

# note that fd has both the zero and N_t indices with real data in each!

frq=np.arange(len(fd))*frqf      # frequency values            

# plot magnitudes vs. freq

plotfft()

######################################################
# this next section gets info in the peaks of interest
#     repeat until an empty line is received

s=input("Approximate peak frequency (empty line quits) > ")

while len(s)>0:       # continue only with non-empty input

# first find the closest peak to the given index
# only works well if the peak is not noisy

    pk0=int(float(s)/frqf)
    
    y0=np.abs(fd[pk0])      
    yp=np.abs(fd[pk0+1])
    ym=np.abs(fd[pk0-1])

    while yp>y0:       # step up until magnitude falls
        ym=y0
        y0=yp
        pk0=pk0+1
        yp=np.abs(fd[pk0+1])

    while ym>y0:       # then, or, go down until mag falls
        yp=y0
        y0=ym
        pk0=pk0-1
        ym=np.abs(fd[pk0-1])

# these points bracket the (theoretically Gaussian) peak, ...

    yp=log(yp)
    y0=log(y0)
    ym=log(ym)

# interpolate to the parobolic (log scale) peak point

    dy=0.5*(yp-ym)
    pk=-dy/(yp-2*y0+ym)
    ypk=exp(y0+0.5*dy*pk)
    pk=pk+float(pk0)
   
# decode the phase from the transform data -- see FFT_check.py for details

    phpk=np.angle(fd[pk0]*np.exp(1j*pi*ns*(float(pk0)-pk)/float(nt)))
    
# plot the local peak phases in a range about the peak -- a flat region
#     indicates a clean, isolated frequency peak

    jph=20
    phs=np.zeros(jph)
    pki=np.zeros(jph,"int32")
    for j in range(jph):
        pki[j]=pk0-jph/2+j
        phs[j]=np.angle(fd[pki[j]]*np.exp(1j*pi*float(ns)*(float(pki[j])-pk)/float(nt)))


    plt.plot(pki*frqf,phs)
    plt.title("RFFT Phase Check")
    plt.grid()
    plt.xlabel("Frequency")
    plt.ylabel("RFFT Phi")
    plt.show()

# print the peak information

    print()
    print("        Peak freq = {:11.4E}".format(pk*frqf))
#    print(  Peak Array Index = {:10.2f}".format(pk))
    print("      Peak height = {:11.4E}".format(ypk))
    print("       Peak Phase = {:7.4f}".format(phpk))
    print()
    
    plotfft()
    
##    plt.plot(frq,np.abs(fd))
##    plt.title("Real FFT Magnitude")
##    plt.grid()
##    plt.xlabel("Scaled Frequency")
##    plt.xscale("log")
##    plt.xlim(0.001,10)
##    plt.yscale("log")
##    #plt.ylim(1E-5,1E-2)
##    plt.ylabel("RFFT Amp")
##    plt.show()

    s=input("Approximate peak frequency (empty line quits) > ")

# done with the loop

print()

