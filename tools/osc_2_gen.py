################
# generate two oscillations
################

# load some packages needed

from math import pi #*                    # all math functions for convenience
import numpy as np                    # useful array operations
import matplotlib.pyplot as plt       # interactive graphics

# Here's the test time series data -- cycles per data set
# cycles per sampled data set: cyc = \omega_0 * (ns*dt) / (2*pi)
# frequency: \omega_0 = 2*pi*cyc/(ns*dt)

ns=1800       # data set size

cyc1=23.4     # osc1
amp1=1
phi1=-2

cyc2=32.4     # osc2
amp2=0.1
phi2=-2

noi=0      # add noise if desired, rms=noi

# generate the data

ts=np.arange(ns)
ts=amp1*np.cos(2*cyc1*pi*ts/ns+phi1)+amp2*np.cos(2*cyc2*pi*ts/ns+phi2)

if noi>0:
    ts=ts+np.random.normal(0,noi,len(ts))    #  (mean, sigma, npts) 

# save the data

np.savetxt("temp.dat", ts , fmt="%16.8E", newline="\n")

# plot the data

plt.title("Time Series")
plt.grid()
plt.xlabel("Sample Index")
plt.plot(ts, "b.",markersize=2)
plt.show()

