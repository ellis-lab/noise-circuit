####################
# simple python plot example
####################

from math import *                  # all math functions
import matplotlib.pyplot as plt     # graphics

# define a function

def my_func(x):
    return sin(exp(x))

# create points in an array


x=[]
y=[]
for i in range(400):
    u=-2+0.01*i
    x=x+[u]
    y=y+[my_func(u)]
    
plt.plot(x,y)
plt.show()

