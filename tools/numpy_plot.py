####################
# simple python plot example using numpy
####################

import numpy as np                  # use numpy arrays
import matplotlib.pyplot as plt     # graphics

# define a function

def my_func(x):
    return np.sin(np.exp(x))*np.exp(-x)

# create points in an array

x=np.arange(-4,4,0.01)
y=my_func(x)

# plot

plt.plot(x,y)
plt.show()

