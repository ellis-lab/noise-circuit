################
# Test calibration of Python's rfft() of a time series data set,
#     with windowing and padding to a power of two.
################

#from math import ceil                    # all math functions (not needed, but
import numpy as np                    # useful array operations
import matplotlib.pyplot as plt       # interactive graphics


#### Here's the test time series data... ####

# cycles per sampled data set: cyc = \omega_0 * (ns*dt) / (2*pi)
# frequency: \omega_0 = 2*pi*cyc/(ns*dt)

ns=1800          # number of sampled data points
cyc=23.4         # frequency in units of cycles per original sample set
amp=1            # amplitude
phi=-2           # phase
noi=0            # add noise if desired...


#### amp * cos( \omega_0 * t + phi ) ####

fj=np.arange(0,ns,dtype=np.float)      # create a numpy array of (floating point) indices
ts=amp*np.cos(2*cyc*np.pi*fj/ns+phi)      # fill with the test data point time series
ts=ts+np.random.normal(0,noi,len(ts))  # add noise (mean, sigma, npts)

# np.savetxt("temp.dat", ts , fmt="%16.8E", newline="\n")    # save the data if you want

###########################################   


# plot the raw data

plt.title("Data Samples")
plt.grid()
plt.xlabel("Point Index")
plt.plot(ts, "b.")
plt.show()


# window the data with a Gaussian, and plot again
# window factor exp(-4) == ~2% at the end-points

rsz=4.0/ns
ts=ts*np.exp(-(rsz*fj-2.0)**2)

plt.title("Gaussian Windowed Data Samples")
plt.grid()
plt.xlabel("Point Index")
plt.plot(ts, "b.")
plt.show()


# pad to the next power of two, = nt

nt=2**int(np.ceil(np.log(ns)/np.log(2)))
ts=np.concatenate((ts,np.zeros(nt-ns)))

plt.title("Data Samples")
plt.grid()
plt.xlabel("Point Index")
plt.plot(ts, "b.")
plt.show()


# take the transform -- use rfft() for real data only!

fd=np.fft.rfft(ts)


# rescale so that the Gaussian peak amplitudes should be = amp
# NOTE -- this is missing the nt factor in "FFT_discussion.pdf" 
#     due to Python-specific rfft() normalization

fd=fd*8.0/ns/np.sqrt(np.pi)


# plot Re, Im parts

plt.plot(fd.real,"r.-",label="Re")
plt.plot(fd.imag,"b.-",label="Im")
plt.legend()
plt.title("RFFT Amplitudes")
plt.grid()
plt.xlabel("Frequency Index")
plt.xscale("log")
plt.ylabel("Output Amp")
plt.show()


# plot magnitude

plt.plot(np.abs(fd),"b.-")
plt.title("RFFT Magnitude")
plt.grid()
plt.xlabel("Frequency Index")
plt.yscale("log")
plt.xscale("log")
plt.ylabel("Output Magnitude")
plt.show()

# get the peak of interest, i.e., closest to the input point index

print()
pk0=int((input("Approximate peak position? ")))
y0=np.abs(fd[pk0])
yp=np.abs(fd[pk0+1])
ym=np.abs(fd[pk0-1])

# step up until the magnitude falls

while yp>y0:    
    ym=y0
    y0=yp
    pk0=pk0+1
    yp=np.abs(fd[pk0+1])

# then, or, go down until magnitude falls

while ym>y0:    
    yp=y0
    y0=ym
    pk0=pk0-1
    ym=np.abs(fd[pk0-1])

# find the ln amp peaks...

yp=np.log(yp)
y0=np.log(y0)
ym=np.log(ym)

# at this point y0 and pk0 are the largest magnotude
# calculate the peak height and floting point index
#     from the peak point and next lower points on both
#     sides, which should be a parabola with a log scale

dy=0.5*(yp-ym)
pk=-dy/(yp-2*y0+ym)
ypk=np.exp(y0+0.5*dy*pk)
pk=pk+float(pk0)
  

# now that the peak point is identified, calculate the peak
#     phase, and plot the calculated phases of the points
#     in a range containing the peak
# remove the analytically derived FFT-specific phase factors
#     form by dividing them out, leaving the e^{i \phi} of the 
#     presumed original data oscillation

phpk=np.angle(fd[pk0]*np.exp(1j*np.pi*ns*(float(pk0)-pk)/float(nt)))

jph=20                      # plot this many points around the peak
##jlo=pk0-jph/2
##if jlo<0:
##    jlo=0
##jhi=pk+jph/2
##if jhi>len(fd):
##    jhi=len(fd)
    
lfd=len(fd)
phs=np.zeros(jph)           # index for phase plot
pki=np.zeros(jph,"int32")
for j in range(jph):
    
    pki[j]=pk0-jph/2+j      # don't go out of range in fd
    if pki[j]<0:
        pki[j]=0
    elif pki[j]>=lfd:
        pki[j]=lfd-1

    phs[j]=np.angle(fd[pki[j]]*np.exp(1j*np.pi*float(ns)*(float(pki[j])-pk)/float(nt)))

plt.plot(pki,phs,"b.-")
plt.title("RFFT Phase")
plt.grid()
plt.xlabel("Frequency Index")
plt.ylabel("RFFT Phi")
plt.show()


# show the peak results -- scale the peak index back to cycles 
#     per ns data sample length = cyc
# amp * cos( \omega_0 * t + phi ) with \omega_0 = 2*pi/T*cyc , T = ns*dt

print()
print("    Peak freq = {:13.6E}  cycles per sample length".format(pk*ns/nt))
print("  Peak height = {:13.6E}  units same as samples".format(ypk))
print("   Peak Phase = {:13.6E}  radians based on t=0 at first point".format(phpk))
print()

