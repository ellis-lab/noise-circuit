import numpy as np
import matplotlib.pyplot as plt

x = np.arange(10,42,2)
i = (x - 8.5) / 4700000
y = np.array([4.3,4.2,4.1,4.1,4.0,3.9,3.9,3.8,3.8,3.8,3.7,3.6,3.7,3.6,3.6,3.5])
b_max = np.array([4.3,5.6,6.3,6.9,7.3,7.7,8.0,8.3,8.5,8.8,8.9,9.0,9.1,9.3,9.5,9.6])

plt.title("Noise Analysis")
plt.grid()
plt.plot(i,y,"bo",i,y,"b",label="10V Bias")
plt.plot(i,b_max,"ro",i,b_max,"r",label="Max")
plt.xlabel("Current (A)")
plt.ylabel("Noise VRMS (mV)")
plt.legend()
plt.show()
