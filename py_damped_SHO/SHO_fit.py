###########################################################
#    Damped SHO fit of a time series sample:               
#    A*cos(wt + phi)*e^(0.5*gamma*wt) + b + s*wt/(2*pi)    
#    wt = 2*pi*n/N                                         
###########################################################

########   file and parameter guesses   ########

# file with column of positions with implicit time

filename = "temp.dat"

# manually supply parameter guesses...

N = 22                  # resonance period (in sample points)
gamma = -0.03           # resonance damping factor
A = 1                   # initial resoance amp
phi = -1                # resonance phase
b = 1                   # background bias
s = 0.1                 # background slope (per cycle)
 
################################################

# start of the fitting script

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import leastsq

# get the data

z = np.loadtxt(filename)
n = np.arange(0,np.size(z))

# define the resonance function (amp and phase relative to RLC resonance)

def Resonance(n, N, gamma, A, phi, b, s):
    cyc=n/N
    wt = 2*np.pi*cyc
    return A*np.cos(wt+phi)*np.exp(0.5*gamma*wt) + b + s*cyc

# define a residual magnitude 
    
def ResErr(N, gamma, A, phi, b, s):
    return np.absolute(Resonance(n, N, gamma, A, phi, b, s)-z)

# check the function

fz=Resonance(n, N, gamma, A, phi, b, s)

plt.xlabel("Point Index")
plt.ylabel("Position")
plt.title("Fit Comparison -- Guesses")
plt.plot(n,z,"ro")
plt.plot(n,fz,"b")
plt.grid()
plt.show()

# define a "lambda" function object to call with a parameter set
#     required by the scipy.optimize.leastsq routine

errfunc = lambda p: ResErr(p[0],p[1],p[2],p[3],p[4],p[5])

# run the least squares (full_output = 0)

p0 = [N, gamma, A, phi, b, s]               # parameter array
p0, flag = leastsq(errfunc, p0[:])          # call scipy.optimize.leastsq

# show the optimized parameters

print()
print(" N_per = %10.4F" % p0[0])
print(" gamma = %14.6E" % p0[1])
print("     A = %14.6E" % p0[2])
print("   phi = %14.6E" % p0[3])
print("     b = %14.6E" % p0[4])
print("     s = %14.6E" % p0[5])

# show the fit with the data

[N, gamma, A, phi, b, s] = p0
fz=Resonance(n, N, gamma, A, phi, b, s)

fz=Resonance(n, N, gamma, A, phi, b, s)

fig = plt.figure()
plt.xlabel("Point Index")
plt.ylabel("Position")
plt.title("Fit Comparison -- Least-Squares")
plt.plot(n,z,"ro")
plt.plot(n,fz,"b")
plt.grid()
plt.show()

# check the fit residuals

ez=ResErr(N, gamma, A, phi, b, s)
print()
print(" RMS Error = %9.3E" % np.sqrt(np.mean(ez*ez)))
print()

plt.xlabel("Point Index")
plt.ylabel("Deviation Magnitude")
plt.title("Fit Residuals")
plt.plot(n,ez)
plt.grid()
plt.show()

# all done

print("##############################################################")
