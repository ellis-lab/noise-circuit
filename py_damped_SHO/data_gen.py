# writes function values to a file 

import numpy as np                    # useful array math
import matplotlib.pyplot as plt       # interactive graphics

# function specs...

nps = 23.456                  # resonance period (in sample points)
gam = -0.1                  # resonance gamma factor
amp = 1.1                   # initial resonance amp
phi = -2.2                  # resonance phase
bac = 0.123                   # background base
slp = 0.03                  # background slope (per radian)
cyc = 6.6                   # number of cycles

sig = 0.03                  # Gaussian noise level

# SHO time series: just the y values, time implicit in the point number

n=np.arange(nps*cyc)

wt=2*np.pi*n/nps
y=amp*np.cos(wt+phi)*np.exp(0.5*gam*wt)+bac+slp*wt+np.random.normal(0,sig,len(n))

np.savetxt("temp.dat",y,fmt="%12.5E")

plt.plot(n,y)
plt.title("Test Data Series")
plt.xlabel("Point Index")
plt.ylabel("Position")
plt.grid()
plt.show()
