# Simple Plot of temp.dat...

cd 'C:\fred\Math\ANALYSIS\Fitting\py_damped_SHO'

set border linewidth 2
set tics scale 2
set title "y(x)" font ",14"
set pointsize 1.5  
#set xtics 5
set xlabel "x" font ",14"
#set logscale x
#set ytics 2
set ylabel "y" font ",14"
set tics font ",14"
#set logscale y
set grid
#set key top right font ",14"

plot "temp.dat" using 0:1 notitle with lines pt 7 lc rgb "blue"
