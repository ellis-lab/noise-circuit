import numpy as np
import matplotlib.pyplot as plt

v=np.array([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15])
f=np.array([3.51,3.86,4.06,4.23,4.33,4.38,4.40,4.42,4.43,4.44,4.44,4.45,4.45,4.45,4.457,4.459])

plt.grid()
plt.plot(v,f,"bo",v,f,"b")
plt.xlabel("Volts")
plt.ylabel("Frequency")
plt.show()
