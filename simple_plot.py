####################
# simple python plot example
####################

from math import *                  # all math functions
import matplotlib.pyplot as plt     # graphics

# define a function

def my_func(x):
    return sin(exp(x))*exp(-x)

# create points in arrays

x=[]
y=[]
for i in range(800):
    u=-4+0.01*i
    x=x+[u]
    y=y+[my_func(u)]

# plot

plt.plot(x,y)
plt.show()

