########################################################################

# point_picker.py

########################################################################

 

import matplotlib.pyplot as plt

import matplotlib.image as mpimg

 

######################################################

# Digitize points from an image, based on an x,y scale

#     and return the x and y value arrays.

######################################################

 

def digitize(imfile):

 

    img = mpimg.imread(imfile)

 

# get LL, UR calibration points...

 

    print()

    print("    Left-click two calibration points:")

    print("        Right click backs up;")

    print("        Hit <ESC> when done.")

    print()

   

    plt.imshow(img)

    [(llpx,llpy),(urpx,urpy)]=plt.ginput(-1,0)

    plt.close()

 

    rsp=input("    Enter LLx, LLy, URx, URy calibration values > ").split(",")

 

    llvx=float(rsp[0])

    llvy=float(rsp[1])

    urvx=float(rsp[2])

    urvy=float(rsp[3])

 

# get the points to be digitized...

 

    print()

    print("    Pick points to digitize, then <ESC> when done.")

 

    plt.imshow(img)

    xyi = plt.ginput(-1,0)

    plt.close()

 

    mx=(urvx-llvx)/(urpx-llpx)

    my=(urvy-llvy)/(urpy-llpy)

 

# apply the scaling and save...

 

    outf=open("points.dat","w")

   

    x=[]

    y=[]

    xyl=len(xyi)

    for j in range(xyl):

        (xi,yi)=xyi[j]

        (xi,yi)=(mx*(xi-llpx)+llvx, my*(yi-llpy)+llvy)

        x.append(xi)

        y.append(yi)

        outf.write("{:11.4E} {:11.4E}\n".format(xi,yi))

#        outf.write("{:11.4E} {:11.4E}\n".format(xi,10**yi))   # restore log scale

 

    outf.close()

 

    print("        ---> {:d} points saved to \"points.dat\".".format(xyl))

    print()

 

    return x,y

 

 

######################################################

# Use the digitize function, plot the points.

######################################################

 

print()

fname=input("    Image file name > ")

 

x,y=digitize(fname)

 

# show the points

 

plt.plot(x,y,"ro")

plt.grid()

plt.show()

 

 

 

 

