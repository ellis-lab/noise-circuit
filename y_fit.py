########################################################################

# y_fit.py

########################################################################

 

# generic y=(x) fit 

 

import numpy as np

import matplotlib.pyplot as plt

from scipy.optimize import leastsq

 

##############################################################

# Everything until "### End Modification ###" needs adjusting

#     for a different function

 

# define the fit function y(x) with parameter set p

 

def y(x, p):

    y=p[0]+p[1]/(1+(x/p[2])**p[3])

    if logvals==True:        # this allows relative mean-square weighting

        return np.log(y)

    else:

        return y

 

 

# data source: file with columns [x], [y] for y(x)...

 

filename = "points.dat" 

 

 

# manually supply parameter names and guesses for func(x) defined later...

 

# example: cap(v) = cp+c0/(1+(v/v0)**a)

 

pn=["cp","c0","v0"," a"]       # parameter labels

p=[3, 150, 2, 1]               # parameter value guesses

 

 

# use this to fit the log of the values for a relative weighting

 

logvals=True

logvals=False

 

 

##############################################################

# start of the fitting script, nothing else to change

 

# get the data

 

xd,yd = np.loadtxt(filename, usecols=(0,1), unpack="true")

 

if logvals==True:

    yd=np.log(yd)

    ylabel="ln(y)"

else:

    ylabel="y"

 

# define a residual error for each point in the (x,y) set

   

def ResErr(p):

    return np.absolute(y(xd,p)-yd)   # sum of squares of errors

 

 

# show the guesses for the function fit

 

fy=y(xd, p)

 

plt.xlabel("x")

plt.ylabel(ylabel)

plt.title("Fit Comparison -- Guesses")

plt.plot(xd,yd,"ro")

plt.plot(xd,fy,"bx",xd,fy,"b")

plt.grid()

plt.show()

 

 

# define a "lambda" function object to call with a parameter set

#     required by the scipy.optimize.leastsq routine

 

errfunc = lambda p: ResErr(p)

 

 

# run the least squares (full_output = 0) returning the optimized p[]

 

p, flag = leastsq(errfunc, p)          # call scipy.optimize.leastsq

 

 

# show the optimized parameters

 

print()

for j in range(len(p)):

    print("   {:s}  = {:14.6E}".format(pn[j],p[j]))

 

 

# show the fit with the data

 

fy=y(xd,p)

 

plt.xlabel("x")

plt.ylabel(ylabel)

plt.title("Fit Comparison -- Guesses")

plt.plot(xd,yd,"ro")

plt.plot(xd,fy,"bx",xd,fy,"b")

plt.grid()

plt.show()

 

 

# check the fit residuals

 

er2=ResErr(p)

er2=er2*er2

 

print()

if logvals==True:

    print(" RMS Log Error = %9.3E" % np.sqrt(np.mean(er2)))

else:

    print(" RMS Error = %9.3E" % np.sqrt(np.mean(er2)))

print()

 

plt.xlabel("x")

plt.ylabel("Fit "+ylabel+" Deviation Magnitude")

plt.title("Fit Residuals")

plt.plot(xd,np.sqrt(er2))

plt.grid()

plt.show()

 

 

# prevents the terminal from closing

 

input(">>> <return> to exit ")

 

########################################################################

########################################################################

