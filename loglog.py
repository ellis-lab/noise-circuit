import numpy as np
import matplotlib.pyplot as plt

# just the numbers pF, MHz..
x=np.array([0,1,2,4,6,8,10,13,15])
y=np.array([3.75,4.09,4.21,4.38,4.41,4.43,4.44,4.45,4.46])


plt.title("log-log graph")
plt.xscale("log")
plt.yscale("log")
plt.grid()
plt.plot(x,y,"bo",x,y,"b")
#plt.plot(c,3/np.sqrt(c/180))
plt.xlabel("V")
plt.ylabel("f")
plt.show()
