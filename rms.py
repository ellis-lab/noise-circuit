import numpy as np
import matplotlib.pyplot as plt

file="dpo2014.dat"

x = np.loadtxt(file)
szx = len(x)
n = np.array([])

for j in range(1250):
    y = x[j*1000:(j*1000+1250)]
    y = y * y
    y_avg = np.sqrt(np.mean(y))
    
    n = np.append(n, y_avg)

n = n[300:1100]

i = np.arange(0.5,6,5.5/(1100-300))

plt.plot(i,n*1000)
plt.grid()
plt.xlim(0,6)
plt.ylim(0,25)
plt.xlabel("I_Zener (mA)")
plt.ylabel("Noise Voltage (mVRMS)")
plt.title("Reverse Biased Zener Diode")
plt.show()
