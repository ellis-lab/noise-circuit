####################
# python plot example using complex functions
####################

import numpy as np                  # use numpy arrays
import matplotlib.pyplot as plt     # graphics

# define a function

def my_func(x):
    return np.sin(np.exp(x))*np.exp(-x)-0.5J   # numpy supports complex functions!

# create points in an array

x=np.arange(-4,4,0.01)
y=my_func((1+0.03J)*x)

# plot with some additional features

plt.grid()
plt.plot(x,y.real,"r")
plt.plot(x,y.imag,"b")
plt.ylim(-2,2)
plt.show()

